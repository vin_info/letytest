<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testRequestLoginLink(): void
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $client->submitForm('Войти', ['email' => 'john.doe@example.com',]);
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorExists('div.alert.alert-info:contains("john.doe@example.com")');
    }
}
