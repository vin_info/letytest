<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DiscountControllerTest extends WebTestCase
{
    public function testIndexUnauthorized(): void
    {
        $client = static::createClient();
        $client->request('GET', '/discount/');

        $this->assertResponseRedirects('/login');
    }

    public function testIndexAuthorizedNoDiscounts(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('john.doe@example.com');

        $client->loginUser($testUser);

        $client->request('GET', '/discount/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('div.alert.alert-light', 'Вы еще не добавили скидки');
    }

    public function testIndexDiscountsList(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('ivan.ivanov@example.com');

        $client->loginUser($testUser);

        $client->request('GET', '/discount/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('td:contains("Тестовая скидка для тестов")');
    }

    public function testNewDiscountUnauthorized()
    {
        $client = static::createClient();
        $client->request('GET', '/discount/new');

        $expiredAt = new \DateTimeImmutable('+3 day');

        $client->submitForm(
            'Добавить',
            [
                'discount[email]' => 'petro.sidorov@example.com',
                'discount[title]' => 'Новая супер пупер скидка',
                'discount[image]' => dirname(__DIR__, 2).'/public/images/test.png',
                'discount[regularPrice]' => 100,
                'discount[discount]' => 10,
                'discount[discountType]' => 'price',
                'discount[url]' => 'https://google.com',
                'discount[whereApply]' => 'site',
                'discount[expiresAt][date][day]' => $expiredAt->format('j'),
                'discount[expiresAt][date][month]' => $expiredAt->format('n'),
                'discount[expiresAt][date][year]' => $expiredAt->format('Y'),
                'discount[expiresAt][time][hour]' => $expiredAt->format('G'),
                'discount[expiresAt][time][minute]' => $expiredAt->format('i'),
                'discount[delivery]' => 'pickup',
                'discount[description]' => 'Тестовое описание сикдки',
            ]
        );
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorExists('td:contains("Новая супер пупер скидка")');
    }

}
