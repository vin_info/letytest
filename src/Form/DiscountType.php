<?php

namespace App\Form;

use App\Entity\Discount;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class DiscountType extends AbstractType
{
    private $security;
    private $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        if (!$user) {
            $builder->add(
                'email',
                EmailType::class,
                [
                    'label' => 'E-mail',
                    'required' => true,
                    'mapped' => false,
                    'constraints' => [
                        new Email(['message' => 'Это значение не является действительным адресом электронной почты.']),
                        new Callback(
                            function ($email, ExecutionContextInterface $context, $payload) use ($options, $builder) {
                                $user = $this->entityManager->getRepository(User::class)->findOneByEmail($email);

                                if ($user) {
                                    $context->addViolation(
                                        'Пользователь с таким адресом электронной почты уже существует. Используйте другой адрес електронной почты или выполните вход в систему'
                                    );
                                }
                            }
                        ),
                    ],
                ]
            );
        }

        $builder
            ->add('title', null, [
                'label' => 'Название',
            ])
            ->add(
                'image',
                FileType::class,
                [
                    'label' => 'Изображение',
                    'help' => 'Необязательно',
                    'required' => false,
                    'mapped' => false,
                    'constraints' => [
                        new Image(['maxSize' => '1024k']),
                    ],
                ]
            )
            ->add(
                'regularPrice',
                MoneyType::class,
                [
                    'label' => 'Обычная цена',
                    'currency' => 'RUB',
                    'invalid_message' => 'Введите число',
                ]
            )
            ->add(
                'discount',
                NumberType::class,
                [
                    'label' => 'Скидка',
                    'scale' => 2,
                    'invalid_message' => 'Введите число',
                ]
            )
            ->add(
                'discountType',
                ChoiceType::class,
                [
                    'label' => 'Тип скидки',
                    'choices' => array_combine(
                        array_map(
                            function ($value) {
                                return 'letytest.discount_type.'.$value;
                            },
                            Discount::DISCOUNT_TYPES
                        ),
                        Discount::DISCOUNT_TYPES
                    ),
                    'expanded' => false,
                    'multiple' => false,
                ]
            )
            ->add('url', null, [
                'label' => 'Ссылка на ресурс',
            ])
            ->add(
                'whereApply',
                ChoiceType::class,
                [
                    'label' => 'Где действует',
                    'choices' => array_combine(
                        array_map(
                            function ($value) {
                                return 'letytest.where_apply.'.$value;
                            },
                            Discount::WHERE_APPLY_TYPES
                        ),
                        Discount::WHERE_APPLY_TYPES
                    ),
                    'placeholder' => 'Выберите где применяется скидка',
                    'help' => 'Необязательно',
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                ]
            )
            ->add(
                'expiresAt',
                DateTimeType::class,
                [
                    'label' => 'Действует до',
                    'input' => 'datetime_immutable',
                    'years' => range((int)date('Y'), (int)date('Y') + 5),
                ]
            )
            ->add(
                'delivery',
                ChoiceType::class,
                [
                    'label' => 'Доставка',
                    'choices' => array_combine(
                        array_map(
                            function ($value) {
                                return 'letytest.delivery.'.$value;
                            },
                            Discount::DELIVERY_TYPES
                        ),
                        Discount::DELIVERY_TYPES
                    ),
                    'expanded' => false,
                    'multiple' => false,
                ]
            )
            ->add('description', null, [
                'label' => 'Описание',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Discount::class,
            ]
        );
    }
}
