<?php

namespace App\Entity;

use App\Repository\DiscountRepository;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AppAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=DiscountRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[AppAssert\NewDiscountCount]
class Discount
{
    const DISCOUNT_TYPES = ['price', 'percent'];
    const DELIVERY_TYPES = ['novaposhta', 'pickup'];
    const WHERE_APPLY_TYPES = ['offline', 'site', 'instagram'];
    const STATUSES = ['pending_moderation', 'published'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private string $uuid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $imageFilename = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(message: 'Введите название')]
    #[Assert\Length(min: 15)]
    private string $title;

    /**
     * @ORM\Column(type="float")
     */
    #[Assert\NotBlank(message: 'Введите обычную цену')]
    #[Assert\Positive(message: 'Введите число больше 0')]
    private float $regularPrice = 0;

    /**
     * @ORM\Column(type="float")
     */
    #[Assert\NotBlank(message: 'Введите скидку')]
    #[Assert\Positive(message: 'Введиет число больше 0')]
    private float $discount = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(message: 'Выберите тип скидки')]
    #[Assert\Choice(choices: Discount::DISCOUNT_TYPES, message: 'Неподдерживаемый тип скидки')]
    private string $discountType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(message: 'Укажите ссылку на сайт')]
    #[Assert\Url(message: 'Неправильная ссылка')]
    private string $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Assert\Choice(choices: Discount::WHERE_APPLY_TYPES, message: 'Неподдерживаемый тип')]
    private ?string $whereApply = null;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    #[Assert\NotBlank(message: 'Укажите до когда действует скидка')]
    #[Assert\GreaterThanOrEqual('+23 hour', message: 'Срок действия должен быть больше или равен {{ compared_value }}.')]
    private \DateTimeImmutable $expiresAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Assert\NotBlank(message: 'Выберите тип доставки')]
    #[Assert\Choice(choices: Discount::DELIVERY_TYPES, message: 'Неподдерживаемый тип доставки')]
    private string $delivery;

    /**
     * @ORM\Column(type="text")
     */
    #[Assert\NotBlank(message: 'Укажите описание')]
    private string $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="discounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct()
    {
        $this->setExpiresAt(new \DateTimeImmutable('+1 day'));
        $this->setStatus('pending_moderation');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @ORM\PrePersist
     */
    public function setUuidValue(): void
    {
        $this->uuid = Uuid::v6();
    }

    public function getImageFilename(): ?string
    {
        return $this->imageFilename;
    }

    public function setImageFilename(?string $imageFilename): self
    {
        $this->imageFilename = $imageFilename;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRegularPrice(): ?float
    {
        return $this->regularPrice;
    }

    public function setRegularPrice(float $regularPrice): self
    {
        $this->regularPrice = $regularPrice;

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountType(): ?string
    {
        return $this->discountType;
    }

    public function setDiscountType(string $discountType): self
    {
        if (!in_array($discountType, self::DISCOUNT_TYPES)) {
            throw new InvalidArgumentException('Unsupported Discount Type "'.$discountType.'"');
        }

        $this->discountType = $discountType;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getWhereApply(): ?string
    {
        return $this->whereApply;
    }

    public function setWhereApply(string $whereApply): self
    {
        if (!in_array($whereApply, self::WHERE_APPLY_TYPES)) {
            throw new InvalidArgumentException('Unsupported Where Apply "'.$whereApply.'"');
        }

        $this->whereApply = $whereApply;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTimeImmutable $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getDelivery(): ?string
    {
        return $this->delivery;
    }

    public function setDelivery(string $delivery): self
    {
        if (!in_array($delivery, self::DELIVERY_TYPES)) {
            throw new InvalidArgumentException('Unsupported Delivery "'.$delivery.'"');
        }

        $this->delivery = $delivery;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    #[Assert\Callback]
    public static function validateDiscount(
        Discount $discount,
        ExecutionContextInterface $context,
        $payload
    ) {
        if ($discount->getDiscountType() === 'percent'
            && ($discount->getDiscount() <= 0 || $discount->getDiscount() > 100)) {
            $context->buildViolation('Маскимальный процент скидки 100 %')
                ->atPath('discount')
                ->addViolation();
        }

        if ($discount->getDiscountType() === 'price'
            && ($discount->getDiscount() > $discount->getRegularPrice() || $discount->getDiscount() <= 0)){
            $context->buildViolation('Скидка должна быть меньше обычной цены')
                ->atPath('discount')
                ->addViolation();
        }
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, self::STATUSES)) {
            throw new InvalidArgumentException('Unsupported Status "'.$status.'"');
        }

        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
