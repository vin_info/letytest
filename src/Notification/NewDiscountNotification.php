<?php


namespace App\Notification;


use App\Entity\Discount;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;

class NewDiscountNotification extends Notification implements EmailNotificationInterface
{
    private $discount;

    public function __construct(Discount $discount)
    {
        $this->discount = $discount;

        parent::__construct('Добавлена скидка');
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $email = (new NotificationEmail())
            ->markAsPublic()
            ->to($recipient->getEmail())
            ->subject($this->getSubject())
            ->content($this->getContent() ?: $this->getSubject());

        $message = new EmailMessage($email);
        $message->getMessage()
            ->htmlTemplate('emails/new_discount_notification.html.twig')
            ->context(['discount' => $this->discount]);

        return $message;
    }
}
