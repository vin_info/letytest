<?php

namespace App\DataFixtures;

use App\Entity\Discount;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmail('john.doe@example.com');
        $manager->persist($user1);

        $user2 = new User();
        $user2->setEmail('ivan.ivanov@example.com');
        $manager->persist($user2);

        $discount1 = new Discount();
        $discount1->setUser($user2)
            ->setTitle('Тестовая скидка для тестов')
            ->setStatus('pending_moderation')
            ->setExpiresAt(new \DateTimeImmutable('+2 day'))
            ->setDelivery('novaposhta')
            ->setDescription('Тестовое описание скидки')
            ->setDiscount(10)
            ->setDiscountType('percent')
            ->setRegularPrice(100)
            ->setUrl('https://google.com')
            ->setWhereApply('offline');
        $manager->persist($discount1);

        $manager->flush();
    }
}
