<?php


namespace App\Menu;


use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;

class MenuBuilder
{
    private FactoryInterface $factory;

    /**
     * Add any other dependency you need...
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root')
            ->setChildrenAttribute('class', 'nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0');

        $menu->addChild(
            'letytest.main_menu.home',
            [
                'route' => 'dashboard',
                'attributes' => ['class' => 'nav-item'],
                'linkAttributes' => ['class' => 'nav-link px-2 link-dark'],
            ]
        );
        $menu->addChild(
            'letytest.main_menu.discounts',
            [
                'route' => 'discount_index',
                'attributes' => ['class' => 'nav-item'],
                'linkAttributes' => ['class' => 'nav-link px-2 link-dark'],
            ]
        );
        $menu->addChild(
            'letytest.main_menu.add_discount',
            [
                'route' => 'discount_new',
                'attributes' => ['class' => 'nav-item'],
                'linkAttributes' => ['class' => 'nav-link px-2 link-dark'],
            ]
        );

        return $menu;
    }
}