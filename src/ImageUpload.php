<?php


namespace App;


use App\Entity\Discount;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUpload
{
    private $imageOptimizer;
    private $discountImageDir;

    public function __construct(ImageOptimizer $imageOptimizer, string $discountImageDir)
    {
        $this->imageOptimizer = $imageOptimizer;
        $this->discountImageDir = $discountImageDir;
    }

    public function handleDiscountImageUpload(?UploadedFile $image, Discount $discount)
    {
        if ($image) {
            $filename = bin2hex(random_bytes(6)).'.'.$image->guessExtension();

            $image->move($this->discountImageDir, $filename);

            $this->imageOptimizer->resize($this->discountImageDir.'/'.$filename);

            $filesystem = new Filesystem();
            $oldDiscountImage = $this->discountImageDir.'/'.$discount->getImageFilename();
            if ($discount->getImageFilename() && $filesystem->exists($oldDiscountImage)) {
                $filesystem->remove($oldDiscountImage);
            }

            $discount->setImageFilename($filename);
        }
    }
}