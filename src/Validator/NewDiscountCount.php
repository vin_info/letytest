<?php


namespace App\Validator;


use Symfony\Component\Validator\Constraint;

#[\Attribute]
class NewDiscountCount extends Constraint
{
    public $message = 'Вы можете добавить не более 5 скидок в сутки';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}