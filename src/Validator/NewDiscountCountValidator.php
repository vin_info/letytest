<?php


namespace App\Validator;


use App\Entity\Discount;
use App\Repository\DiscountRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NewDiscountCountValidator extends ConstraintValidator
{
    private $security;
    private $discountRepository;

    public function __construct(DiscountRepository $discountRepository, Security $security)
    {
        $this->security = $security;
        $this->discountRepository = $discountRepository;
    }

    /**
     * @param Discount $discount
     * @param Constraint $constraint
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function validate($discount, Constraint $constraint)
    {
        if (!$constraint instanceof NewDiscountCount) {
            throw new UnexpectedTypeException($constraint, NewDiscountCount::class);
        }


        $user = $this->security->getUser();
        if ($user) {
            $discountsCount = $this->discountRepository->createQueryBuilder('d')
                ->select('COUNT(d.id)')
                ->andWhere('d.user = :user')
                ->andWhere('d.createdAt > :yesterday')
                ->andWhere('d.createdAt < :tomorrow')
                ->setParameters([
                    'user' => $user,
                    'yesterday' => new \DateTimeImmutable('yesterday'),
                    'tomorrow' => new \DateTimeImmutable('tomorrow'),
                ])
                ->getQuery()
                ->getSingleScalarResult();

            if ($discountsCount >= 5){
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}