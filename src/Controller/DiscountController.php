<?php

namespace App\Controller;

use App\Entity\Discount;
use App\Entity\User;
use App\Form\DiscountType;
use App\ImageUpload;
use App\Notification\NewDiscountNotification;
use App\Repository\DiscountRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authenticator\Token\PostAuthenticationToken;

#[Route('/discount')]
class DiscountController extends AbstractController
{
    #[Route('/', name: 'discount_index', methods: ['GET'])]
    public function index(
        DiscountRepository $discountRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $query = $discountRepository->createQueryBuilder('d')
            ->where('d.user = :user')
            ->setParameter('user', $this->getUser())
            ->getQuery();

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            25
        );

        return $this->render(
            'discount/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

    #[Route('/new', name: 'discount_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        NotifierInterface $notifier,
        ImageUpload $imageUpload
    ): Response {
        $discount = new Discount();
        $form = $this->createForm(DiscountType::class, $discount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();
            $entityManager = $this->getDoctrine()->getManager();

            if (!$user) {
                $user = new User();
                $user->setEmail($form['email']->getData());
                $entityManager->persist($user);
                $entityManager->flush();

                $token = new PostAuthenticationToken($user, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));
            }

            $discount->setUser($user);

            $imageUpload->handleDiscountImageUpload($form['image']->getData(), $discount);

            $entityManager->persist($discount);
            $entityManager->flush();

            $notification = new NewDiscountNotification($discount);
            $notification->importance(Notification::IMPORTANCE_LOW);

            $notifier->send($notification, new Recipient($user->getEmail()));

            return $this->redirectToRoute('discount_index');
        }

        return $this->render(
            'discount/new.html.twig',
            [
                'discount' => $discount,
                'form' => $form->createView(),
            ]
        );
    }

    #[Route('/{uuid}', name: 'discount_show', methods: ['GET'])]
    public function show(
        Discount $discount
    ): Response {
        if ($discount->getUser() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

        return $this->render(
            'discount/show.html.twig',
            [
                'discount' => $discount,
            ]
        );
    }
}
